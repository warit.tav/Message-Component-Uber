import React, { PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class SimpleMessage extends PureComponent {
  render() {
    const { title, message } = this.props;
    return (
      <View style={styles.container}>
        <Text style={[styles.text, styles.title]}>
          {title}
        </Text>
        <Text style={styles.text}>
          {message}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    marginBottom: 10,
    padding: 15,
  },
  text: {
    color: '#000',
    fontFamily: 'Avenir',
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 10,
  },
});